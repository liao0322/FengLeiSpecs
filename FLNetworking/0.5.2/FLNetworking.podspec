#
# Be sure to run `pod lib lint FLNetworking.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FLNetworking'
  s.version          = '0.5.2'
  s.summary          = 'FLNetworking.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Add long description of the pod here.
                       DESC

  s.homepage         = 'https://coding.net/u/liao0322/p/FLNetworking/git?public=true'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'liao0322' => 'liao0322@hotmail.com' }
  s.source           = { :git => 'https://git.coding.net/liao0322/FLNetworking.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'FLNetworking/Classes/**/*'
  
  # s.resource_bundles = {
  #   'FLNetworking' => ['FLNetworking/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
s.dependency 'MJExtension', '~> 3.0.13'
s.dependency 'AFNetworking', '~> 3.1.0'
s.dependency 'FLManagers'
s.dependency 'FLCategories'
s.dependency 'FLBaseClasses'
s.dependency 'FLUtils'

end
