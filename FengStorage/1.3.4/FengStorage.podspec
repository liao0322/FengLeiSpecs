#
# Be sure to run `pod lib lint FengStorage.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FengStorage'
  s.version          = '1.3.4'
  s.summary          = 'A short description of FengStorage.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'http://192.168.1.99:30000/feelee/FengStorage'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'liao0322' => 'liao0322@hotmail.com' }
  s.source           = { :git => 'http://192.168.1.99:30000/feelee/FengStorage.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

#s.source_files = 'FengStorage/Classes/**/*'
  s.source_files = 'FengStorage/Classes/**/*.{h,m}'
  s.resource_bundles = {
    'FengStorage' => ['FengStorage/Assets/*.png', 'FengStorage/Classes/**/*.xib', 'FengStorage/Classes/ZhiFuBao/AlipaySDK.bundle']
  }
  
  s.ios.vendored_frameworks = 'FengStorage/Classes/ZhiFuBao/AlipaySDK.framework'
  #s.ios.vendored_libraries = 'LTVoiceAssistant/Classes/libBDVoiceRecognitionClient.a'
  s.ios.vendored_libraries = 'FengStorage/Classes/WeChatSDK/*.{a}'
  s.libraries = 'c++','z','sqlite3','iconv' #'sqlite3.0',
  s.frameworks = 'SystemConfiguration','CoreTelephony','QuartzCore','CoreGraphics','CoreText','CFNetwork','CoreMotion'

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'Masonry', '~> 1.1.0'
  s.dependency 'DZNEmptyDataSet', '~> 1.8.1'
  s.dependency 'SDWebImage'
  #s.dependency 'FLBaseClasses'
  s.dependency 'FLNetworking'
  s.dependency 'SDCycleScrollView', '~> 1.62'
  s.dependency 'SDAutoLayout', '~> 2.1.8'
  s.dependency 'FLCategories'
  s.dependency 'GoodsDetails_Category'
  s.dependency 'TimeBuying_Category'
  s.dependency 'NewProducts_Category'
  s.dependency 'WholesaleOrderDetails_Category'
  s.dependency 'CloudStoreOrderDetails_Category'
  s.dependency 'FengXiangHomePage_Category'
  s.dependency 'SearchResults_Category'
  s.dependency 'HomeNewsDetails_Category'
  #s.dependency 'Tangram'
  s.dependency 'HMSegmentedControl'
  s.dependency 'XFWidgets'
  s.dependency 'Prepayments_Category'
  s.dependency 'FLManagers'
  s.dependency 'FLSubtitleMenuView'
  s.dependency 'XFPasswordTextField'
  #s.dependency 'WechatOpenSDK', '~> 1.7.9'
  

  
  
  
  
end
