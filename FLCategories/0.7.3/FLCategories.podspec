#
# Be sure to run `pod lib lint FLCategories.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FLCategories'
  s.version          = '0.7.3'
  s.summary          = 'FLCategories.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Add long description of the pod here.
                       DESC

# s.homepage         = 'http//192.168.1.99:30000/feelee/ios-components-sourcecode'
s.homepage         = 'https://gitlab.com/liao0322/FLCategories'

  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'liao0322' => 'liao0322@hotmail.com' }
  # s.source           = { :git => 'http://192.168.1.99:30000/feelee/ios-components-sourcecode.git', :tag => s.version.to_s }
  s.source           = { :git => 'https://gitlab.com/liao0322/FLCategories.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'FLCategories/Classes/**/*'
  
  # s.resource_bundles = {
  #   'FLCategories' => ['FLCategories/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'GTMBase64'
end
